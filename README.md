# Bonanza

Una aplicaci�n web para anuncios clasificados. Llam�mosla nom�s "app" para no escribir tanto.


### Usuarios


1. **Buscante**, **lector**. Es una persona que se conecta desde su ordenador o su m�vil. Ve anuncios del sitio. Lo puede hacer de manera an�nima o se puede identificar con el sitio.

2. **Anunciante**. Puede hacer todo lo que un **Buscante**, pero no es an�nimo. Tiene correo electr�nico, se identifica con el sitio. Y puede someter anuncios.

3. **Administrador**. Es un usuario identificado por el sistema con capacidad de alterar directamente tablas en la base de datos, y con facultades de alterar la tabla de usuarios y de alterar contenidos aportados por otros usuarios.
    
### Casos de uso:

1. **Filtrar** anuncios. Un **Buscante** o un **Anunciante** pueden especificar criterios para enlistar anuncios. Criterios pueden ser: por lugar (ciudad, pueblo, region), por categor�a (Ropa, enseres, bicicletas, etc.), por precio (solo entre 10 y 20 euro), y otros que hay que pensar.

2. **Revisar** un anuncio a detalle. Al elegir un anuncio de entre la lista, el **lector**-**Buscante** puede ver una descripci�n y las fotos que haya subido el **Anunciante**.

3. **Recomendar** a otros: Al poseer un enlace directo a cada producto el **Anunciante** (no se puede recomendar de manera an�nima) puede recomendar anuncios a sus familiares y amigos. Hay dos formas de recomendar, una fuera del sitio y otra dentro. Para la forma fuera del sitio, cada anuncio tiene un URL �nico (bonanza.cu/anuncio/234234), y estos se pueden enviar por correo o por chat o como sea. Para la forma dentro del sitio, cada anuncio tiene un bot�n de "recomendar", tras presionarlo el sitio pide un correo (TODO: investigar otros canales) y el sitio mismo env�a un correo: "tal persona piensa que podria interesarte este anuncio". Adem�s de enviar ese correo, se registra en una tabla de recomendaciones: 1. quien recomienda 2. qu� recomienda 3. qu� categor�as tiene el anuncio 4. a qui�n lo recomienda (nombre, correo) 5. fecha y hora

4. **Crear** anuncios. Un **Anunciante** puede describir un producto o servicio que quiere comprar, rentar, vender, donar. Un anuncio debe tener t�tulo y descripci�n. Opcionalmente se puede especificar: precio, fotos. Se le pueden poner etiquetas para clasificaci�n. Se debe elegir una categor�a para el anuncio.


5. **Publicar** anuncio. El ciclo de vida de un anuncio es: 1. creado 2. publicado 3. expirado 4. cancelado 5. removido. Un **Anunciante** puede pasar sus propios anuncios de estado 1. creado a 2. publicado, de publicado a removido o a cancelado. Autom�ticamente sus anuncios publicados pasan a estado expirado.

6. **Promocionar** anuncio. A cambio de dinero ponemos tu anuncio arriba de resultados de b�squeda. (por ejemplo).
7.**Crear** un perfil. Un **Anunciante** puede describir sus intereses, lo que suele comprar o vender. Esto aumenta la probabilidad de encontrar lo que busca por nuestro avanzado sistema de anuncios personalizados.

8. **Registrarse** como **Anunciante**. Un usuario an�nimo nos da su correo electr�nico y elige una contrase�a. 

9. **Recuperar** contrase�a perdida. El usuario nos da una direcci�n, le enviamos instrucciones de c�mo restablecer su contrase�a perdida.

10. **Marcar** anuncios como favoritos. El **lector**-buscador registrado puede entrar a un apartado en su perfil donde encuentra una lista de los anuncios que ha marcado. As� puede volver a ella en otra sesi�n. Tambi�n sirve para crear un perfil del **lector**-buscador: qu� cosas le interesa comprar, cu�nto dinero est� dispuesto a gastar en ellas.

11. **Poner** en suspension cuentas. Este uso s�lo un **Administrador**.

12. **Poner** en suspensi�n contenido. **Administrador**. Por motivos como:
        - Inapropiado para los usuarios
        - ilegal
        -etc.
    El **Administrador** puede elegir un anuncio y suspenderlo. No lo borra de la base de datos pero de inmediato se oculta a los **lector**es. Se toma nota del **Anunciante**, para amonestarlo o suspenderlo o a ver qu�.
    